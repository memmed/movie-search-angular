import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from 'events';
import { MoviesService } from '../services/movies.service';
@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  @Input('name') name: string;
  @Output() voted = new EventEmitter<boolean>();

  constructor(
    private movie: MoviesService
  ) { }

  ngOnInit() {
    console.log(this.name);
    this.movie.test().subscribe(
      data => {
        console.log(data);
      }
    );
  }



  uploadFile(event): void{
    // let reader = new FileReader();

    // reader.readAsDataURL(event.target.files[0]);

    // reader.onload = (event) => {
    //   console.log(event);
    //   let image = document.createElement('img');
    //   image.setAttribute('src', event.target.result);
    //   let spot = document.getElementById('image_spot');
    //   spot.appendChild(image);

    // }
  }

  test(){
    this.voted.emit(true);
    console.log(name);
  }

}
