import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ImageComponent } from "./image/image.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { NoteComponent } from "./note/note.component";
import { MessageComponent } from "./message/message.component";

const appRoutes: Routes = [
  {
    path: 'image',
    component: ImageComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'notes',
    component: NoteComponent
  },
  {
    path: 'messages',
    component: MessageComponent
  },
  {
    path: 'messages/:username',
    component: MessageComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule{

}