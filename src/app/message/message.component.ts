import { Component, OnInit } from '@angular/core';
import * as IO from 'socket.io-client';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  private socket;
  private typing = false;
  inbox = false;
  users: Array<any>;

  constructor(
    private http: Http,
    private route: ActivatedRoute
  ) { }

  installScripts(){
    // let script = document.createElement('script');
    // script.src = "./assets/main.js";
    // document.body.appendChild(script);
  }


  ngOnInit() {
    // this.installScripts();
    this.route.params.subscribe(param => {
      let username = param['username'];
      if(username){
        this.inbox = true;
      }
    })
    this.socket = IO('http://localhost:3000/');
    setInterval(() => {
      if(this.typing) this.typing = false;
    }, 5000);
    this.http.get('http://localhost:3200/api/users')
      .map(res => res.json())
      .subscribe(data => {
        this.users = data;
      })
  }

  sendMessage(e): void{
    this.typing = true;
   
    
    if(e.keyCode == 13){
      
      this.socket.emit('sent', {
        message: e.target.value
      });
      e.target.value = null;
    } 
  }

}
