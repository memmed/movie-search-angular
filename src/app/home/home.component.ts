import { Component, OnInit } from '@angular/core';
import { IncluderService } from '../services/includer.service';

import 'sweetalert2/src/sweetalert2.scss';
import swal from 'sweetalert2';
import {  } from 'sweetalert2';
import { ChatService } from '../services/chat.service';
import { Http } from '@angular/http';
import * as IO from 'socket.io-client';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  constructor(
    private Helper: IncluderService,
    private Chat: ChatService,
    private http: Http
  ) { 
    
  }

  ngOnInit() {
    if(this.Helper.cookie.read('personal_user_token') == null){
      this.Helper.router.navigate(['/login']);
    }
    this.http.get('http://127.0.0.1:3200/api/')
      .map(res => res.json())
      .subscribe(data => {
        console.log(data);
      });

    let socket = IO('http://localhost:3000/');
    // socket.on('connection', (client) => {
    //   console.log(client);
    // });

    // socket.on('typing', (data) => {
    //   console.log(data);
    // })

    // socket.on('Hello', (data) => {
    //   console.log(data);
    // });

  }


  logout(){
    swal({
      title: 'Are you sure to log out?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Log out'
    }).then((result) => {
      if (result.value) {
        this.Helper.auth.logout().subscribe(data => {
          this.Helper.cookie.remove("personal_user_token");
          this.Helper.router.navigate(['/login']);
          const toast = (swal as any).mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          
          toast({
            type: 'success',
            title: 'Logged out successfully'
          });
        }, error => {
          console.error(error);
        })
      }
    })
    
  }

}
