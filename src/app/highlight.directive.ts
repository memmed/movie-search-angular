import { Directive, ElementRef, HostListener, Host, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

 

  @HostListener('mouseenter') abc(){
    this.changeColor('yellow');
  }

  @HostListener('mouseleave') test(){
    this.changeColor('red');
  }

  constructor(
    private el: ElementRef
  ) { 
    el.nativeElement.style.backgroundColor = "red";
  }

  changeColor(color: string){
    this.el.nativeElement.style.backgroundColor = color;
  }

}
