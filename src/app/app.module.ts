import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { AppBootstrapModule } from './app-bootstrap.module';
import { MoviesComponent } from './movies/movies.component';


import { ImageComponent } from './image/image.component';
import { HighlightDirective } from './highlight.directive';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';

import { ReactiveFormsModule } from '@angular/forms';

import { MoviesService } from './services/movies.service';
import { AuthService } from './services/auth.service'
import { CookieService } from './services/cookie.service';
import { IncluderService } from './services/includer.service';
import { HomeComponent } from './home/home.component';
import { NoteService } from './services/note.service';
import { NoteComponent } from './note/note.component';
import { MessagesService } from './services/messages.service';
import { ChatService } from './services/chat.service';
import { MessageComponent } from './message/message.component';


@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    ImageComponent,
    HighlightDirective,
    LoginComponent,
    HomeComponent,
    NoteComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    AppBootstrapModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    MoviesService, 
    AuthService, 
    CookieService,
    IncluderService,
    NoteService,
    MessagesService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
