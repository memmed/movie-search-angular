import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IncluderService } from '../services/includer.service';
import { NoteService } from '../services/note.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';


@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  @ViewChild("swalInput1") swalInput1: ElementRef
  @ViewChild("swalInput2") swalInput2: ElementRef

  constructor(
    private note: NoteService
  ) { }

  notes: Array<any>;

  ngOnInit() {  
    this.note.getAll().subscribe(data => {
      this.notes = data['data']
    });

  }


  async create(){
    const {value: formValues} = await swal({
      title: 'Create note',
      html:
        '<input id="swal-input1" class="swal2-input">' +
        '<input id="swal-input2" class="swal2-input">',
      allowEnterKey: true,
      focusConfirm: false,
      confirmButtonText: 'Create',
      preConfirm: () => {
        return [
          (<HTMLInputElement>document.getElementById('swal-input1')).value,
          (<HTMLInputElement>document.getElementById('swal-input2')).value
        ]
      },
      allowOutsideClick: () => !swal.isLoading()
    })
    if(formValues){
      let data = { 
        "header": formValues[0],
        "text": formValues[1]
      };
      this.note.insert(data)
        .subscribe(data => {
          this.notes.push(data['data'])
        });
    }
  }

  remove(id: number){
    swal({
      title: 'Are you sure want to delete?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete'
    }).then((result) => {
      if (result.value) {
        
        this.note.remove(id).subscribe(data => {
          if(data['status'] == 200){
            for(let i = 0; i < this.notes.length; i++){
              if(this.notes[i]['note_id'] == id){
                this.notes.splice(i, 1);
              }
            }
          }
        });
      }
    })
  }
}
