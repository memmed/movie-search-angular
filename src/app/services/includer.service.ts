import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { CookieService } from './cookie.service';
import { AuthService } from './auth.service';

@Injectable()
export class IncluderService {

  public url: string = "http://localhost/personal/";

  constructor(
    public http: Http,
    public router: Router,
    public auth: AuthService,
    public cookie: CookieService
  ) { }

}
