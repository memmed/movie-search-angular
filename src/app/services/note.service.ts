import { Injectable } from '@angular/core';
import { IncluderService } from './includer.service';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';

@Injectable()
export class NoteService {

  private header: Headers;

  constructor(
    private helper: IncluderService
  ) { }

  getAll(): Observable<any>{
    this.header = new Headers();
    let token = this.helper.cookie.read('personal_user_token');
    this.header.append('personal_user_token', token);
    return this.helper.http.get(`${this.helper.url}notes.php`, {
      headers: this.header
    }).map(
        res => res.json()
      );
  }

  insert(data: any): Observable<any>{
    this.header = new Headers();
    let token = this.helper.cookie.read('personal_user_token');
    this.header.append('personal_user_token', token);
    return this.helper.http.post(`${this.helper.url}notes.php`,data, {
      headers: this.header
    }).map(
      res => res.json()
    );
  }

  remove(id: number){
    this.header = new Headers();
    let token = this.helper.cookie.read('personal_user_token');
    this.header.append('personal_user_token', token);
    return this.helper.http.delete(`${this.helper.url}notes.php?id=${id}`, {
      headers: this.header
    }).map(
      res => res.json()
    );
  }
}
