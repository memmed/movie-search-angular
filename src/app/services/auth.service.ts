import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from './cookie.service';
@Injectable()
export class AuthService {

  private header: Headers;

  constructor(
    private http: Http,
    private cookie: CookieService
  ) { 
  }

  private url: string = "http://localhost/personal/";

  login(data: Array<any>){

    return this.http.post(`${this.url}?type=login`, data)
      .map(
        res => res.json(),
        msg => {
          console.error(msg);
        }
      ); 
  }

  logout(){
    this.header = new Headers();
    let token = this.cookie.read('personal_user_token');
    this.header.append('personal_user_token', token);
    let data = {};
    console.log(this.header);
    return this.http.post(`${this.url}?type=logout`, data,{
      headers: this.header
    }).map(
      res => res.json()
    );
  }

}
