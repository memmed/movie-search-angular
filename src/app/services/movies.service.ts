import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class MoviesService {

  private url: string = "http://www.omdbapi.com/";
  private api_key: string = "apikey=767ff14f"

  constructor(
    private http: Http
  ) { }

  getMovie(title: string): Observable<any>{
    return this.http.get(`${this.url}?t=${title}&${this.api_key}`)
      .map(res => res.json());
  }
  search(title: string){
    let promise = new Promise((resolve, reject) => {
      this.http.get(`${this.url}?t=${title}&${this.api_key}`)
        .toPromise()
        .then(
          res => res.json(),
          msg => reject()
        )
    });
  }

  test(){
    return this.http.get(`http://localhost/personal/?type=song`)
      .map(res => res.json());
  }

}