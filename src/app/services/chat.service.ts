import { Injectable } from '@angular/core';
import { MessagesService } from './messages.service';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class ChatService {


  messages: Subject<any>;

  constructor(
    private message: MessagesService
  ) { 
    // this.messages = <Subject<any>>message
    //   .connect()
    //   .map((response: any): any => {
    //     return response;
    //   });
  }

  sendMessage(msg){
    this.messages.next(msg);
  }

}
