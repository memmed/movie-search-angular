import { Component, OnInit, Input, SimpleChange } from '@angular/core';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  @Input() q: string = "";
  
  public Movies: any[] = Array();

  constructor(
    private movies_service: MoviesService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: {[q: string]: SimpleChange}): void {
    if(changes['q'] && changes['q'].previousValue != changes['q'].currentValue){
      this.q = changes['q'].currentValue;
      if(this.q !== ""){
        this.fetchData(this.q);
      }
    }
  }

  fetchData(title: string){
    this.movies_service.getMovie(title)
      .subscribe(data => {
        this.Movies = [];
        this.Movies.push(data);
      });
  }

}
