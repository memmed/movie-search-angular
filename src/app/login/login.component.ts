import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { CookieService } from '../services/cookie.service';
import { IncluderService } from '../services/includer.service';

import 'sweetalert2/src/sweetalert2.scss';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  private token_name: string = "personal_user_token";
  public wrong_password: boolean = false;

  constructor(
    private fb: FormBuilder,
    private helper: IncluderService
  ) { }

  ngOnInit() {
    if(this.helper.cookie.read(this.token_name) !== null){
      this.helper.router.navigate(['']);
    }
    this.createForm();
    
  }

  createForm(){
    this.loginForm = this.fb.group({
      username: '',
      password: ''
    });
  }

  login(){
    this.helper.auth.login(this.loginForm.value)
      .subscribe(data => {
        if(data['status'] == 200){
          
          this.helper.cookie.write(this.token_name, data['data']['user_token']);
          this.helper.router.navigate(['']);

          const toast = (swal as any).mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          
          toast({
            type: 'success',
            title: 'Signed in successfully'
          });
        }else if(data['status'] == 300){
          this.wrong_password = true;
        }
      });
  }

}
