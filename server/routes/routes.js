const express = require('express');
const router = express.Router();
const mysql = require('mysql');

const axios = require('axios');
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "personal"
});
db.connect((err) => {
  if(err){
    console.log(err);
  }
  console.log("Mysql Connection was successfull");
})
router.get('/', (req, res) => {

  let data = {
    name: 'Memo',
    father: 'Emin',
    mother: 'Mehpare'
  };
  res.json(data);
  // res.send('route works');

});

router.get('/users', (req, res) => {
  
  var query = `SELECT 
                user_id,
                user_name,
                user_surname,
                user_username
                FROM users`;
                // let query = "SELECT * FROM users";
  db.query(query, (err, results) => {
    if(!err){
      res.json(results);
    }else{
      console.log(err);
    }
  })

  
});

module.exports = router;